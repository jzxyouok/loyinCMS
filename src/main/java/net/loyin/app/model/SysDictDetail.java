package net.loyin.app.model;

import net.loyin.annotation.TableBind;
import net.loyin.app.model.base.BaseSysDictDetail;

/**
 * Generated by JFinal.
 */
@SuppressWarnings("serial")
@TableBind(tableName = SysDictDetail.tableName)
public class SysDictDetail extends BaseSysDictDetail<SysDictDetail> {
	public static final String tableName="sys_dict_detail";
	public static final SysDictDetail dao = new SysDictDetail();
}
