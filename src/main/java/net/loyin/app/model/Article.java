package net.loyin.app.model;

import net.loyin.annotation.TableBind;
import net.loyin.app.model.base.BaseArticle;

/**
 * 文章
 */
@SuppressWarnings("serial")
@TableBind(tableName = Article.tableName)
public class Article extends BaseArticle<Article> {
	public static final String tableName="article";
	public static final Article dao = new Article();
}
