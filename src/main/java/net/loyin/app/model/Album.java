package net.loyin.app.model;

import net.loyin.annotation.TableBind;
import net.loyin.app.model.base.BaseAlbum;

/**
 * 相册
 */
@TableBind(tableName = Album.tableName)
public class Album extends BaseAlbum<Album> {
	public static final String tableName="album";
	public static final Album dao = new Album();
}
