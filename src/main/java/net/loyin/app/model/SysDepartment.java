package net.loyin.app.model;

import net.loyin.annotation.TableBind;
import net.loyin.app.model.base.BaseSysDepartment;

/**
 * Generated by JFinal.
 */
@SuppressWarnings("serial")
@TableBind(tableName = SysDepartment.tableName)
public class SysDepartment extends BaseSysDepartment<SysDepartment> {
	public static final String tableName="sys_department";
	public static final SysDepartment dao = new SysDepartment();
}
