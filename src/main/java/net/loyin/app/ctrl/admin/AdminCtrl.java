package net.loyin.app.ctrl.admin;

import com.jfinal.aop.Clear;
import com.jfinal.kit.HashKit;
import com.jfinal.kit.PropKit;
import com.jfinal.kit.StrKit;
import com.jfinal.plugin.ehcache.CacheKit;
import net.loyin.annotation.ControllerBind;
import net.loyin.app.Constant;
import net.loyin.app.model.SysMenu;
import net.loyin.app.model.SysUser;
import net.loyin.utils.encrypt.DESUtils;
import org.apache.commons.lang.StringUtils;

import java.util.Date;

/**
 * 管理后台
 * Created by loyin on 16/1/12.
 */
@ControllerBind(route = "/admin")
public class AdminCtrl extends AdminBaseCtrl {
    private static boolean useCaptcha = true;

    @Clear
    public void index() {
        if (StringUtils.isNotBlank(this.getUserId())) {
            this.redirect("/admin/main");
        }
        useCaptcha = PropKit.getBoolean("system.admin.captcha", true);
        this.setAttr("useCaptcha", useCaptcha);
        String para=this.getPara(0);
        if(StringUtils.isNotBlank(para)){
            this.renderHtml("<h1>未找到/"+para+"</h1>");
        }
    }

    public void dashboard() {

    }

    @Clear
    public void login() {
        String username = this.getPara("username");
        String password = this.getPara("password");
        String vcode = this.getPara("vcode");
        String captcha_code = this.getCookie("_jfinal_captcha");
        this.removeCookie("_jfinal_captcha");
        if (useCaptcha)
            if (StrKit.isBlank(captcha_code)) {
                this.renderJsonMsg(this.JSON_RESULT_WARNNING,"验证码超时!请刷新验证码.");
                return;
            }
        if (useCaptcha)
            if (StrKit.isBlank(vcode)) {
                this.renderJsonMsg(this.JSON_RESULT_WARNNING,"验证码不能为空!");
                return;
            }
        if (StrKit.isBlank(username)) {
            this.renderJsonMsg(this.JSON_RESULT_WARNNING,"用户名不能为空!");
            return;
        }
        if (StrKit.isBlank(password)) {
            this.renderJsonMsg(this.JSON_RESULT_WARNNING,"密码不能为空!");
            return;
        }
        if (useCaptcha)
            if (StringUtils.isNotBlank(vcode) && StringUtils.isNotBlank(captcha_code)) {
                if (captcha_code.equals(HashKit.md5(vcode)) == false) {
                    this.renderJsonMsg(this.JSON_RESULT_WARNNING,"验证码错误!");
                    return;
                }
            }
        DESUtils desUtils = new DESUtils();
        SysUser user = SysUser.dao.login(username, desUtils.encryptString(password));
        if (user == null) {
            this.renderJsonMsg(this.JSON_RESULT_WARNNING,"用户名和密码不匹配!");
            return;
        } else {
            String userid = user.getId();
            user.clear();
            user.setId(userid);
            user.setLastLoginTime(new Date());
            user.setLastLoginIp(this.getRemortIP());
            user.update();
            this.setCookie(PropKit.get(Constant.LOGIN_COOKIE_KEY), desUtils.encryptString(userid), 864000);
            this.renderJsonMsg(this.JSON_RESULT_SUCCESS,null);
            return;
        }
    }

    @Clear
    public void logout() {
        try {
            this.removeCookie(PropKit.get(Constant.LOGIN_COOKIE_KEY));
        } catch (Exception e) {
        }
        this.redirect("/admin");
    }
    /**登录超时*/
    @Clear
    public void timeOut(){
    }
    /**无权限*/
    @Clear
    public void noPermission(){
    }
    public void main() {
        if (StringUtils.isBlank(this.getUserId())) {
            this.redirect("/admin");
            return;
        }
        this.setAttr("menuList", SysMenu.dao.getAuthMenu(this.getUserId()));
    }

    /**
     * 验证码图片
     */
    @Clear
    public void captcha() {
        this.renderCaptcha();
    }

    /***
     * 清除缓存
     */
    public void clearCache() {
        CacheKit.remove(this.getPara(0), this.getPara(1));
    }
}
