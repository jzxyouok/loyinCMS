package net.loyin.app.ctrl.admin;

import net.loyin.annotation.ControllerBind;
import net.loyin.annotation.CtrlMethod;
import net.loyin.app.model.NavMenu;
import org.apache.commons.lang.StringUtils;

/**
 * Created by loyin on 16/1/14.
 */
@ControllerBind(route = "/admin/navMenu")
public class NavMenuCtrl extends AdminBaseCtrl{
    public void index(){
        this.setAttr("dataList", NavMenu.dao.find("select * from nav_menu order by sort asc"));
    }
    public void add(){
        String id=this.getPara(0);
        this.setAttr("parentid",this.getPara(1));
        this.setAttr("levelno",this.getPara(2));
        if(StringUtils.isNotBlank(id)){
            this.setAttr("po",NavMenu.dao.findById(id));
        }
    }
    @CtrlMethod(isAjax = true)
    public void save(){
        NavMenu entity=this.getModel(NavMenu.class);
        if(entity!=null){
            String id=entity.getId();
            if(StringUtils.isEmpty(id)) {
                entity.save();
            }else{
                entity.update();
            }
            this.renderJsonMsg(JSON_RESULT_SUCCESS,"保存成功!");
        }else{
            this.renderJsonMsg(JSON_RESULT_ERROR,"保存失败!");
        }
    }
    @CtrlMethod(isAjax = true)
    public void del(){
        String id=this.getPara(0);
        try {
            if (StringUtils.isNotBlank(id)) {
                NavMenu.dao.deleteById(id);
                this.renderJsonMsg(JSON_RESULT_SUCCESS, "删除成功!");
            } else {
                this.renderJsonMsg(JSON_RESULT_WARNNING, "缺少参数!");
            }
        }catch (Exception e){
            logger.error("删除异常",e);
            this.renderJsonMsg(JSON_RESULT_ERROR, "删除异常,可能存在子导航,请先删除子导航!");
        }
    }
}
