package net.loyin.app.ctrl.front;

import com.jfinal.aop.Clear;
import net.loyin.annotation.ControllerBind;

/**
 * 前端首页
 * Created by loyin on 16/1/12.
 */
@Clear
@ControllerBind(route = "/")
public class FrontCtrl extends FrontBaseCtrl{
    public void index(){
        this.setAttr("name",this.getPara(0));
//        this.render("index2");
    }
    public void ajax(){
        this.renderJson("{\"sd\":\"sdfsdf\"}");
    }
}
