package net.loyin.app.ctrl.front;

import com.jfinal.aop.Before;
import com.jfinal.aop.Clear;
import com.jfinal.core.Controller;
import net.loyin.app.icp.AdminInterceptor;
import net.loyin.app.icp.FrontInterceptor;
import org.apache.log4j.Logger;

/**
 * 前端基础ctrl类
 * Created by loyin on 16/1/12.
 */
@Clear(AdminInterceptor.class)
@Before(FrontInterceptor.class)
public abstract class FrontBaseCtrl extends Controller {
    public Logger logger= Logger.getLogger(this.getClass());
}
