package net.loyin.dev;

import javax.sql.DataSource;
import com.jfinal.kit.PathKit;
import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.dialect.PostgreSqlDialect;
import com.jfinal.plugin.activerecord.generator.Generator;
import com.jfinal.plugin.c3p0.C3p0Plugin;
import com.jfinal.plugin.druid.DruidPlugin;
import net.loyin.app.Constant;

/**
 * GeneratorDemo
 */
public class GeneratorDemo {
	
	public static DataSource getDataSource() {
		Prop p = PropKit.use("config.txt");
		String dbtype=PropKit.get(Constant.DB_TYPE);

		DruidPlugin druidPlugin=new DruidPlugin(PropKit.get(dbtype+"."+ Constant.DB_URL),
				PropKit.get(dbtype+"."+Constant.DB_USERNAME),
				PropKit.get(dbtype+"."+Constant.DB_PASSWORD),
				PropKit.get(dbtype+"."+Constant.DB_DRIVER));
		druidPlugin.start();
		return druidPlugin.getDataSource();
	}
	
	public static void main(String[] args) {
		// base model 所使用的包名
		String baseModelPackageName = "net.loyin.app.model.base";
		// base model 文件保存路径
		String baseModelOutputDir = PathKit.getWebRootPath().replace("src/main/webapp","javaCode") + "/net/loyin/app/model/base";
		System.out.println(baseModelOutputDir);
		// model 所使用的包名 (MappingKit 默认使用的包名)
		String modelPackageName = "net.loyin.app.model";
		// model 文件保存路径 (MappingKit 与 DataDictionary 文件默认保存路径)
		String modelOutputDir = baseModelOutputDir.replace("/base","");
		
		// 创建生成器
		Generator gernerator = new Generator(getDataSource(), baseModelPackageName, baseModelOutputDir, modelPackageName, modelOutputDir);
		gernerator.setDialect(new PostgreSqlDialect());
		// 添加不需要生成的表名
		gernerator.addExcludedTable("sys_user_role");
		gernerator.addExcludedTable("sys_role_menu");
		// 设置是否在 Model 中生成 dao 对象
		gernerator.setGenerateDaoInModel(true);
		// 设置是否生成字典文件
		gernerator.setGenerateDataDictionary(false);
		// 设置需要被移除的表名前缀用于生成modelName。例如表名 "osc_user"，移除前缀 "osc_"后生成的model名为 "User"而非 OscUser
//		gernerator.setRemovedTableNamePrefixes("t_");
		// 生成
		gernerator.generate();
	}
}




