package net.loyin.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * controller的method标注
 * Created by loyin on 16/1/12.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
public @interface CtrlMethod {
    /**菜单关联*/
    String menuKey() default "";
    boolean isAjax() default false;
    String newView() default "";
}
